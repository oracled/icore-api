var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
const chalk = require("chalk");
const logger = require("./logger");

var indexRouter = require('./routes/index');

var app = express();

app.use((req, res, next) => {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    res.on('finish',() => {
        if (res.statusCode<400) {
            logger.log(chalk.green(req.method, req.url, res.statusCode));
        }
        if (res.statusCode<500 && res.statusCode >=400) {
            logger.warn(chalk.yellow(req.method, req.url, res.statusCode));
        }
        if (res.statusCode>=500) {
            logger.error(chalk.red(req.method, req.url, res.statusCode));
        }
    });
    next();
});
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({
      error: res.locals.message,
      stack: res.locals.error,
      status: err.status || 500,
      url: req.url,
      sent: new Date()
  });
});

module.exports = app;
