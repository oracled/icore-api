const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    name: {type: String, required: true},
    abbreviation:{type: String, required: true},
    logo_url: {type: String, required: true}
});

module.exports = {
    name: "ICO",
    schema
};