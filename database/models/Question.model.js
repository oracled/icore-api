const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    text: {type: String, required: true, long: true},
    help_text: {type: String, required: true},
    level: {type: Number, required: true},
    soft_cap: {type: Number, required: true},
    hard_cap: {type: Number, required: true},
    est_minutes: {type: Number, required: true},
    type: {
        type: String,
        enum: ["rating", "binary", "short_text", "long_text"],
        required: true
    },
    relativity: {
        type: String,
        enum: ["objective", "subjective"]
    },
    points: {type: Number, required: true},
    regular_bonus: {type: Boolean, required: true, default: false},
    reputational_bonus: {type: Boolean, required: true, default: false},
    knockout: {type: Boolean, required: true, default: false},
    position: {type: Number, required: true},
    priority: {type: Number, required: true}
});

module.exports = {
    name: "Question",
    schema
};