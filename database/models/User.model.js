const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    username: {type: String, required: true},
    email: {type: String, required: true},
    password: {type: String},
    created: {type: Date, required: true, default: Date.now()},
    birthday: {type: Date, required: true},
    full_name: {type: String, required: true},
    country: {type: String, required: true},
    account_number: {type: String},
    account_name: {type: String},
    account_sort: {type: String},
    reputation: {type: Number, required: true, default: 0},
    level: {type: Number, required: true, default: 0},
    groups: [{
        type: String
    }]
});

module.exports = {
    name: "User",
    schema
};