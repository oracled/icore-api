const glob = require("glob");

const models = glob.sync("database/models/*.model.js", {});

let Models = [];

models.forEach(model => {
    Models.push({
        name: model.replace("database/models/","").replace(".model.js",""),
        schema: require("./"+model.replace("database/models/","")).schema
    })
});

module.exports = Models;