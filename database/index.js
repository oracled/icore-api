const mongoose = require('mongoose');
const models = require('./models');

mongoose.connect('mongodb://localhost/icore', {useCreateIndex: true,useNewUrlParser: true});

let collections = {};

models.forEach(model => {
    collections[model.name] =  mongoose.model(model.name, model.schema);
});

collections.Close = () => {
    mongoose.disconnect();
};

collections.getCollections = () => {
    return Object.keys(collections).filter(x => x!=="Close"&&x!=="getCollections");
};

module.exports = collections;
