const winston = require('winston');

const logFormat = winston.format.printf(info => {
    return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
});

const logger = winston.createLogger({
    format: winston.format.combine(

        winston.format.colorize(),
        winston.format.label({label: 'ICORE_API'}),
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        logFormat
    ),
    transports: [
        new winston.transports.Console()
    ]
});

let handler = {
    log: (message, data) => {
        logger.log('info', message, data)
    },
    error: (message, data) => {
        if (typeof message === "object") {
            message = JSON.stringify(message);
        }
        logger.log('error', message, data)
    },
    warn: (message, data) => {
        if (typeof message === "object") {
            message = JSON.stringify(message);
        }
        logger.log('warn', message, data)
    }
};

module.exports = handler;