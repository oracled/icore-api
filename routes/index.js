var express = require('express');
var router = express.Router();
const database = require('../database');
const pluralize = require('pluralize');
const logger = require('../logger');

database.getCollections().forEach(collection => {
    router.get('/'+pluralize(collection)+'.json', (req, res) => {
        let {page = 0} = req.query;

        page = parseInt(page);

        let skip = page * 15;
        if (page > 0) {
            skip -= 15;
        }

        database[collection].find({}, {}, {skip: skip, limit: 15}, (err, result) => {
            res.json(result)
        })
    })
});


module.exports = router;
